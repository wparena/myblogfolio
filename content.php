<?php 
/**
 * The template for displaying the content.
 * @package myblogfolio
 */
?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="myblogblog-post-box">
			<?php
				if (('post' === get_post_type() && !post_password_required()) && (comments_open() || get_comments_number())) :
					if ((has_post_thumbnail()) || (!has_post_thumbnail() && !is_single())) :
						?>
						<div class="comments-count">
							<a href="<?php esc_url(comments_link()); ?>">
								<?php
								printf(_nx('<i class="fa fa-comment"></i> 1', '<i class="fa fa-comment"></i> %1$s', get_comments_number(), 'comments title', 'myblogfolio'), number_format_i18n(get_comments_number()));
								?>
							</a>
						</div>
						<?php
					endif;
				endif;
			?>
			<article class="small">
				<h1><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>">
				  <?php the_title(); ?>
				  </a>
				</h1>
				<div class="myblogblog-category post-meta-data"> 
					
					<span><?php echo get_the_date( 'F j, Y' ); ?></span>

					| Posted in<a href="#">
					  <?php   $cat_list = get_the_category_list();
					  if(!empty($cat_list)) { ?>
					  <?php the_category(', '); ?>
					</a>
					<?php } ?>
					
				</div>
				<hr>
				<p>
					<?php
						echo get_the_excerpt();
					?>
				</p>
					<?php wp_link_pages( array( 'before' => '<div class="link">' . __( 'Pages:', 'myblogfolio' ), 'after' => '</div>' ) ); ?>
			</article>
		</div>
	</div>
