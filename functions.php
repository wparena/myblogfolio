<?php
/**
 * myblogfolio functions and definitions.
 *
 *
 * @package myblogfolio
 */


	$myblogfolio_theme_path = get_template_directory() . '/inc/';

	require( $myblogfolio_theme_path . '/custom-navwalker.php' );
	require( $myblogfolio_theme_path . '/font/font.php');

	/*-----------------------------------------------------------------------------------*/
	/*	Enqueue scripts and styles.
	/*-----------------------------------------------------------------------------------*/
	require( $myblogfolio_theme_path .'/enqueue.php');
	/* ----------------------------------------------------------------------------------- */
	/* Customizer */
	/* ----------------------------------------------------------------------------------- */

	require( $myblogfolio_theme_path . '/customize/customize_copyright.php');
	require( $myblogfolio_theme_path . '/customize/customize_control/class-customize-alpha-color-control.php');
	
	
	
	

if ( ! function_exists( 'myblogfolio_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function myblogfolio_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on myblogfolio, use a find and replace
	 * to change 'myblogfolio' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'myblogfolio', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __('Primary menu','myblogfolio' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	
	//Custom Logo
	add_theme_support( 'custom-logo');

	function myblogfolio_the_custom_logo() {
		the_custom_logo();
	}

	add_filter('get_custom_logo','myblogfolio_logo_class');


	function myblogfolio_logo_class($html)
	{
	$html = str_replace('custom-logo-link', 'navbar-brand', $html);
	return $html;
	}

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'myblogfolio_custom_background_args', array(
		'default-color' => 'eeeeee',
		'default-image' => '',
	) ) );

    // Set up the woocommerce feature.
    add_theme_support( 'woocommerce');

}
endif;
add_action( 'after_setup_theme', 'myblogfolio_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function myblogfolio_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'myblogfolio_content_width', 640 );
}
add_action( 'after_setup_theme', 'myblogfolio_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function myblogfolio_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'myblogfolio' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="myblogwidget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6>',
		'after_title'   => '</h6>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget Area', 'myblogfolio' ),
		'id'            => 'footer_widget_area',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="col-md-5 col-sm-5 rotateInDownLeft animated myblogwidget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6>',
		'after_title'   => '</h6>',
	) );
}
add_action( 'widgets_init', 'myblogfolio_widgets_init' );


function myblogfolio_enqueue_customizer_controls_styles() {
  wp_register_style( 'myblogfolio-customizer-controls', get_template_directory_uri() . '/css/customizer-controls.css', NULL, NULL, 'all' );
  wp_enqueue_style( 'myblogfolio-customizer-controls' );
  }
add_action( 'customize_controls_print_styles', 'myblogfolio_enqueue_customizer_controls_styles' );


/* Custom template tags for this theme. */
require get_template_directory() . '/inc/template-tags.php';

//Read more Button on slider & Post
function myblogfolio_read_more() {
	
	global $post;
	
	$readbtnurl = '<br><a class="btn btn-tislider-two" href="' . get_permalink() . '">'.__('Read More','myblogfolio').'</a>';
	
    return $readbtnurl;
}
add_filter( 'the_content_more_link', 'myblogfolio_read_more' );

// Changing excerpt more
   function myblogfolio_excerpt_more($more) {
   	if ( is_admin() ){
		return $more;
	}
   	global $post;
   	return ' <a href="'. get_permalink($post->ID) . '">' . __('[...] Read More &raquo;','myblogfolio') . '</a>';
   	}
   	add_filter('excerpt_more', 'myblogfolio_excerpt_more');