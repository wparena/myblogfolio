<?php 
	
	$sticky = get_option( 'sticky_posts' );
	$args = array(
		'posts_per_page' => 1,
		'post__in'  => $sticky,
		'ignore_sticky_posts' => 1
	);
	$myquery = new WP_Query( $args );
?>

<?php if($myquery->have_posts())
		{
		    while($myquery->have_posts()) { $myquery->the_post(); ?>

<div class="article-content featured-post">
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="myblogblog-post-box">
						<div class="row">
							<a href="<?php the_permalink(); ?>" class="myblogblog-thumb col-lg-8 col-md-8 col-xs-8">

						        <?php if(has_post_thumbnail()): ?>
							        <?php $defalt_arg =array('class' => "img-responsive"); ?>
							        <?php the_post_thumbnail('', $defalt_arg); ?>
						        <?php endif; ?>
						    </a>

						<article class="small col-md-4">
							<h1><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>">
							  <?php the_title(); ?>
							  </a>
							</h1>
							<div class="myblogblog-category post-meta-data"> 
								<span><?php echo get_the_date( 'F j, Y' ); ?></span>

								| Posted in<a href="#">
								  <?php   $cat_list = get_the_category_list();
								  if(!empty($cat_list)) { ?>
								  <?php the_category(', '); ?>
								</a>
								<?php } ?>
								
							</div>
							<hr>
							<p>
								<?php
									echo get_the_excerpt();
								?>
							</p>
						</article>
					</div>
					</div>
			</div>
</div>	

	<?php
		 } }
	?>

<?php if($myquery->have_posts()){
		wp_reset_postdata(); 
	}
?>