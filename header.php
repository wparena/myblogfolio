<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package myblogfolio
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="wrapper">
  <div class="container">
    <div class="row">
      <header class="col-lg-12">

        <div class="clearfix"></div>
        <div class="myblogmain-nav">
            <div class="">
              <div class="row">
                  <div class="col-md-6 col-lg-6">
                    <div class="navbar-header">
                    <!-- Logo -->
                    <?php
                    if(has_custom_logo())
                    {
                    // Display the Custom Logo
                    the_custom_logo();
                    }
                     else { ?>
                    <a class="navbar-brand" href="<?php echo esc_url(home_url( '/' )); ?>"><span class="site-title"><?php bloginfo('name'); ?></span>
                    <br>
                    <span class="site-description"><?php echo  get_bloginfo( 'description', 'display' ); ?></span>   
                    </a>      
                    <?php } ?>
                    <!-- Logo -->
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-6 hidden-xs hidden-sm text-right social-nav">
                    <ul class="myblogsocial">
                      <p class="updates">Follow Us </p>
                                 <?php if(get_theme_mod('social_link_facebook','#')) { ?>
                      <li><span class="icon-soci"> <a href="<?php echo esc_url(get_theme_mod('social_link_facebook')); ?>" <?php if(get_theme_mod('Social_link_facebook_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-facebook"></i></a></span></li>
                      <?php } if(get_theme_mod('social_link_twitter','#')) { ?>
                      <li><span class="icon-soci"><a href="<?php echo esc_url(get_theme_mod('social_link_twitter')); ?>" <?php if(get_theme_mod('Social_link_twitter_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-twitter"></i></a></span></li>
                      <?php } if(get_theme_mod('social_link_linkedin','#')) { ?>
                      <li><span class="icon-soci"><a href="<?php echo esc_url(get_theme_mod('social_link_linkedin')); ?>" <?php if(get_theme_mod('Social_link_linkedin_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-linkedin"></i></a></span></li>
                      <?php } if(get_theme_mod('social_link_google','#')) { ?>
                      <li><span class="icon-soci"><a href="<?php echo esc_url(get_theme_mod('social_link_google')); ?>" <?php if(get_theme_mod('Social_link_google_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-google-plus"></i></a></span></li>
                      <?php } ?>
                    </ul>
                  </div>
              </div>
            </div>
        </div>
      </header>
    </div>
<!-- #masthead -->
