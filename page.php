<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @package myblogfolio
 */
get_header(); ?>
<main id="content" class="">

	<?php get_template_part('navbar','');?>
  	<div class="main-content-container">
	<?php get_template_part('featured',''); ?>

      <div class="row">
		<!-- Blog Area -->
			<div class="col-md-9 col-lg-9">
				<div class="page-content">
					<?php if( have_posts()) :  the_post(); ?>		
					<?php the_content(); ?>
					<?php endif; ?>
					<?php wp_link_pages( array( 'before' => '<div class="link page-break-links">' . __( 'Pages:', 'myblogfolio' ), 'after' => '</div>' ) ); ?>
					<?php comments_template( '', true ); // show comments ?>
					<!-- /Blog Area -->
				</div>			
			</div>
			<!--Sidebar Area-->
			<aside class="col-md-3 col-lg-3">
				<?php get_sidebar(); ?>
			</aside>
			<!--Sidebar Area-->
			</div>
	</div>
</main>
<?php
get_footer();