<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @package myblogfolio
 */ ?>
<!--==================== myblogFOOTER AREA ====================-->
  <div class="row">
      <footer class="col-md-12"> 
        <div class="overlay"> 
        <!--Start myblogfooter-widget-area-->
        <?php if ( is_active_sidebar( 'footer_widget_area' ) ) { ?>
        <div class="myblogfooter-widget-area">
              <?php  dynamic_sidebar( 'footer_widget_area' ); ?>
        </div>
        <?php } ?>
        <!--End myblogfooter-widget-area-->
        <div class="myblogfooter-copyright">
            <div class="row">
              
              <div class="text-center hidden-md hidden-lg col-xs-12">
      			    <ul class="myblogsocial">
                             <?php if(get_theme_mod('social_link_facebook','#')) { ?>
                  <li><span class="icon-soci"> <a href="<?php echo esc_url(get_theme_mod('social_link_facebook')); ?>" <?php if(get_theme_mod('Social_link_facebook_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-facebook"></i></a></span></li>
                  <?php } if(get_theme_mod('social_link_twitter','#')) { ?>
                  <li><span class="icon-soci"><a href="<?php echo esc_url(get_theme_mod('social_link_twitter')); ?>" <?php if(get_theme_mod('Social_link_twitter_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-twitter"></i></a></span></li>
                  <?php } if(get_theme_mod('social_link_linkedin','#')) { ?>
                  <li><span class="icon-soci"><a href="<?php echo esc_url(get_theme_mod('social_link_linkedin')); ?>" <?php if(get_theme_mod('Social_link_linkedin_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-linkedin"></i></a></span></li>
                  <?php } if(get_theme_mod('social_link_google','#')) { ?>
                  <li><span class="icon-soci"><a href="<?php echo esc_url(get_theme_mod('social_link_google')); ?>" <?php if(get_theme_mod('Social_link_google_tab')==1){ echo "target='_blank'"; } ?> ><i class="fa fa-google-plus"></i></a></span></li>
                  <?php } ?>
                </ul>
              </div>
              
            </div>
        </div>
        </div>
      </footer>
      <div class="clearfix"></div>
      <div class="text-center copyright">
        <p>Copyright &copy; <?php echo esc_html(date('Y')).' '; bloginfo( 'name' ); ?> | <?php printf( esc_html__( 'Theme by %1$s', 'myblogfolio' ),  '<a href="'.esc_url('https://wpcount.com/').'" rel="designer">WPCount</a>' ); ?>
        </p>
      </div>
    </div>
    </div>
  </div>

<!--Scroll To Top--> 
<a href="#" class="ti_scroll bounceInRight  animated"><i class="fa fa-angle-double-up"></i></a> 
<!--/Scroll To Top-->
<?php wp_footer(); ?>
</body>
</html>